﻿#include <iostream>


bool Deg(int a)
{
	if (a % 2 == 0)
		if (a / 2 > 1)
			Deg(a/2);
		else return  true;
	else return false;

}

int main()
{
	setlocale(LC_ALL, "Rus");
	int a;
	std::cout << "Введите число для проверки: " << std::endl;

	std::cin >> a;

	if (Deg(a))
		std::cout << "Число является степенью двойки" << std::endl;
	else
		std::cout << "Число не является степенью двойки"<<std::endl;

	return 0;
}